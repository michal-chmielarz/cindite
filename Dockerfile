FROM python:3

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /cindite

COPY requirements.txt /cindite/
RUN pip install -r requirements.txt

COPY . /cindite/