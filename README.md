# CINDITE - Commercial Index For Distributed Teams (Django web app)

![Project Logo](docs/images/logo64x64.png "Project Logo")

An application which helps distributed teams obtain consistent and unique commercial codes for further usage in product commercial setup. Django web app created in Apr/May 2022 as a portfolio sample backend application.

> [www.cindite.michalchmielarz.pl](https://cindite.michalchmielarz.pl)

# Dependencies
django==4.0.3  
djangorestframework==3.13.1  
psycopg2==2.9.3  
gunicorn==20.1.0  
requests==2.27.1

# Development
Python 3.10.4 virtual env  
Visual Studio Code  
Django  
Django REST Framework  
Git  
SQLite  
Postman  
Pytest (in progress - for testing sample please see [another project](https://gitlab.com/michal-chmielarz/pay-authorisation))  
Bootstrap 5.1.3

# Production  
Web app (Dockerfile)  
PostgreSQL (Docker)   
AWS Elastic Compute Cloud   
AWS S3 static hosting  
HTTP server: Gunicorn  
Proxy server: Nginx  

# Deployment
**The deployment process is a combination of own favourite best practices with intention to keep them concise and clear. Includes:**
* full integration under Gitlab CI/CD pipeline
* automated creation of source bundle
* multicontainer solution put together with docker-compose
* automated deployment via AWS CLI
* AWS EC2 instance managed via AWS Elastic Beanstalk
* access to running containers via SSH
* a Gunicorn HTTP server 
* static files hosted from a separate public AWS S3 bucket

# Hosting with SSL ![SSL](docs/images/ssl24x24.png "SSL")  

HTTPS  
SSL Certificate  
custom domain  

# Business logic in a nutshell

**Services** are offered in variants. Service variant is called a Product. Users are not allowed to duplicate Service codes or Variant codes thus resulting Products are also unique.

**Products** do not exist on their own. They always depend on a parent Service-Variant pair.

> **Service + Variant = Product**

**Index** is a unique identifier of a product collection created for specific business purpose. Index reference is used to explicitly tag files, e-mail subjects and other details across the systems.


# Permissions (GUI & API)

|            	| Admin 	| Logged User 	| Anonymous 	|
|------------	|-------	|-------------	|-----------	|
| View admin 	| yes   	| yes         	| yes       	|
| View own   	| yes   	| yes         	|           	|
| View any   	| yes   	|             	|           	|
| Create     	| yes   	| yes         	|           	|
| Update own 	| yes   	| yes         	|           	|
| Update any 	| yes   	|             	|           	|
| Delete own 	| yes   	| yes         	|           	|
| Delete any 	| yes   	|             	|           	|

**Codes** act as unique billing identifiers. Therefore, there are field specific rules that apply to all users:
- code fields are blocked for editing once created,
- the only way to edit a code is to remove the whole entry and raise it once again under a new id,
- users are not allowed to duplicate codes but are allowed to duplicate names as long as the code is different,
- Product code remains attached to its parent codes throughout the Product's lifetime but the name might get changed due to business reasons (companies being aquired, merged etc.)

# Restrictions

## Custom classes responsible for access restriction

| Action     | GUI                         | API                            |
|------------|-----------------------------|--------------------------------|
| View any   | QuerySetMixin               | QuerySetMixin                  |
| Create     | FormAuthenticationTestMixin | IsAuthenticatedOwnerOrReadOnly |
| Update any | PermissionTestMixin         | IsAuthenticatedOwnerOrReadOnly |
| Delete any | PermissionTestMixin         | IsAuthenticatedOwnerOrReadOnly |

| Action       	| GUI                                	| API                           	|
|----------------------	|------------------------------------	|-------------------------------	|
| Codes: no updates    	| *Model*UpdateForm (readonly: True) 	| *Model*SerializerWithReadOnly* 	|
| Codes: no duplicates 	| PreventDuplicateCodesMixin         	| PerformCreateMixin            	|

*For Product model, the class additionally prevents from updates on parent codes.

# Application background

## Case Study
A team of around 30 analysts spread across several countries worldwide set up commercial requirements for company products. Their daily task is to create bunches of codes, pricing tables and other dependencies. The configuration they create has to be maintained under specific, unique id & service codes.

**Problem**  
Given the distributed nature of the team, the members have to communicate each time one sets a new product. First they tried exchanging e-mails but that did not work. Currently they maintain records on a shared drive but the solution is a subject to human errors - typos, inconsistencies, interfering entry numbers. There is a complex IT solution underway which includes facilitating their job, unfortunately the system may take up to 2 years before it becomes fully operational.

**Solution**  
A temporary, minimalistic web app which keeps control of all product codes being raised and ensures correct details are in place. At later stage, the app would become a part of the bigger project underway.

## Requirements
- must distribute unique commercial entries and tags
- must ensure the service codes are consistent 
- must provide API to integrate with complex solution at later stage
- should give basic overview statistics allowing to track workload per user
