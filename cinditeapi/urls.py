from django.urls import path, include

from rest_framework.routers import SimpleRouter
from rest_framework.authtoken import views

from cinditeapi.views import ServiceViewSet, VariantViewSet, ProductViewSet, StatsView

router = SimpleRouter()

router.register("service", ServiceViewSet, basename="service-api")
router.register("variant", VariantViewSet, basename="variant-api")
router.register("product", ProductViewSet, basename="product-api")

urlpatterns = router.urls

urlpatterns += [
    path('api-auth/', include('rest_framework.urls')),
    path('login/', views.obtain_auth_token, name="login-api"),
    path('stats/', StatsView.as_view(), name="stats-api")
]