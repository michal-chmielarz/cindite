from rest_framework import serializers


class CustomPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    '''
    Provides queryset to PrimaryKeyRelatedField.
    Whenever the serializer accesses the queryset, the regular CustomQuerySet db manager is used.
    '''
    def get_queryset(self):
        view = self.context.get('view')
        return super().get_queryset().restrict(view)