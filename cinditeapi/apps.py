from django.apps import AppConfig


class CinditeapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cinditeapi'
