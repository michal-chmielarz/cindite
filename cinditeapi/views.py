from rest_framework import views, viewsets
from rest_framework.response import Response
from cinditeapi.stats import StatsCounter

from cinditeapp.models import Service, Variant, Product
from cinditeapp.mixins import QuerySetMixin
from cinditeapi.mixins import PerformCreateMixin
from cinditeapi.permissions import IsSuperuser, IsAuthenticatedOwnerOrReadOnly
from cinditeapi.serializers import (ServiceSerializer, 
                                    ServiceSerializerWithReadOnly, 
                                    VariantSerializer, 
                                    VariantSerializerWithReadOnly, 
                                    ProductSerializer, 
                                    ProductSerializerWithReadOnly,
                                    StatsSerializer,
                                    StatsDateSerializer,)

# Create your views here.
class ServiceViewSet(QuerySetMixin, PerformCreateMixin, viewsets.ModelViewSet):
    model = Service
    serializer_class = ServiceSerializer
    permission_classes = [IsSuperuser|IsAuthenticatedOwnerOrReadOnly]

    def get_serializer_class(self):
        if self.request.method in ('PUT', 'PATCH'):
            return ServiceSerializerWithReadOnly
        return self.serializer_class


class VariantViewSet(QuerySetMixin, PerformCreateMixin, viewsets.ModelViewSet):
    model = Variant
    serializer_class = VariantSerializer
    permission_classes = [IsSuperuser|IsAuthenticatedOwnerOrReadOnly]

    def get_serializer_class(self):
        if self.request.method in ('PUT', 'PATCH'):
            return VariantSerializerWithReadOnly
        return self.serializer_class


class ProductViewSet(QuerySetMixin, viewsets.ModelViewSet):
    model = Product
    serializer_class = ProductSerializer
    permission_classes = [IsSuperuser|IsAuthenticatedOwnerOrReadOnly]

    def get_serializer_class(self):
        if self.request.method in ('PUT', 'PATCH'):
            return ProductSerializerWithReadOnly
        return self.serializer_class


class StatsView(views.APIView):
    '''
    Count of entries created per user in a given period of time.
    Helps track employee workloads.
    '''
    def get(self, request, *args, **kwargs):
        '''
        {
                "user_id": <user_id>,
                "username": "<user_name>",
                "services": <count>,
                "variants": <count>,
                "products": <count>
        }
        '''
        dates_serializer = StatsDateSerializer(data=request.query_params)
        dates_serializer.is_valid(raise_exception=True)

        counter = StatsCounter(
            from_date=dates_serializer.validated_data.get('from_date'),
            to_date=dates_serializer.validated_data.get('to_date'),
            user=self.request.user
            )

        data = StatsSerializer(
                {
                    'results': counter.count(),
                    'date': {'from_date': counter.from_date, 'to_date': counter.to_date}
                }
            ).data
        return Response(data)
