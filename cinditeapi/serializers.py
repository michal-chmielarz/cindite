from django.core import exceptions
from rest_framework import serializers

from cinditeapp.models import Service, Variant, Product
from cinditeapi.fields import CustomPrimaryKeyRelatedField


class ServiceSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Service
        fields = (
            'id',
            'code',
            'name',
            'created',
            'owner',
        )


class ServiceSerializerWithReadOnly(ServiceSerializer):
    class Meta(ServiceSerializer.Meta):
        read_only_fields = ('code',)


class ServiceNestedSerializer(ServiceSerializer):
    # enables Product creation upon existing Service ids
    id = CustomPrimaryKeyRelatedField(queryset=Service.objects)

    class Meta(ServiceSerializer.Meta):
        read_only_fields = ('code',)
        fields = (
            'id',
            'code'
        )


class VariantSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Variant
        fields = (
            'id',
            'code',
            'name',
            'created',
            'owner',
        )


class VariantSerializerWithReadOnly(VariantSerializer):
    class Meta(VariantSerializer.Meta):
        read_only_fields = ('code',)


class VariantNestedSerializer(VariantSerializer):
    # enables Product creation upon existing Variant ids
    id = CustomPrimaryKeyRelatedField(queryset=Variant.objects)

    class Meta(VariantSerializer.Meta):
        read_only_fields = ('code',)
        fields = (
            'id',
            'code'
        )


class ProductSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    service = ServiceNestedSerializer()
    variant = VariantNestedSerializer()

    class Meta:
        model = Product
        read_only_fields = ('code', 'name')
        fields = (
            'id',
            'index_id',
            'service',
            'variant',
            'code',
            'name',
            'created',
            'owner',
        )

    def create(self, validated_data):
        request = self.context['request']
        service = validated_data['service']['id']
        variant = validated_data['variant']['id']
        try:
            product_instance = Product.create_one(request, service, variant)
        except exceptions.ValidationError as e:
            raise serializers.ValidationError(e)
        return product_instance


class ProductSerializerWithReadOnly(ProductSerializer):
    service = ServiceNestedSerializer(read_only=True)
    variant = VariantNestedSerializer(read_only=True)

    class Meta(ProductSerializer.Meta):
        read_only_fields = ('code',)


# /stats/ related serializers

class StatsDateSerializer(serializers.Serializer):
    from_date = serializers.DateField(required=False)
    to_date = serializers.DateField(required=False)


class StatsResultSerializer(serializers.Serializer):
    user_id = serializers.IntegerField()
    username = serializers.CharField()
    services = serializers.IntegerField()
    variants = serializers.IntegerField()
    products = serializers.IntegerField()


class StatsSerializer(serializers.Serializer):
    results = serializers.ListSerializer(child=StatsResultSerializer())
    date = StatsDateSerializer()

