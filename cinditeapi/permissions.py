from rest_framework.permissions import BasePermission, IsAuthenticatedOrReadOnly, SAFE_METHODS
from django.http import Http404


class IsSuperuser(BasePermission):
    '''
    Gives all permissions to a superuser.
    '''
    def has_permission(self, request, view):
        return bool(
            request.user and 
            request.user.is_superuser
        )


class IsAuthenticatedOwnerOrReadOnly(IsAuthenticatedOrReadOnly):
    '''
    Extends standard IsAuthenticatedOrReadOnly class by adding object-level owner check.
    Object permission for "safe method" or authenticated owner.
    If not an object then follow the standard route.
    '''
    def has_permission(self, request, view):
        if view.kwargs.get('pk'):
            obj = view.get_object()
            
            return bool(
                request.method in SAFE_METHODS or
                request.user and
                request.user.is_authenticated and
                request.user == obj.owner
            )

        return super().has_permission(request, view)