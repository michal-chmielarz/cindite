from cinditeapp.models import Service, Variant, Product, User
from django.db.models import Q

class StatsCounter:
    '''
    Returns stats for a userlist.
    '''
    def __init__(self, from_date, to_date, user):
        self.from_date = from_date
        self.to_date = to_date
        self.user = user

        filters = {}
        if self.from_date:
            filters['created__gte'] = self.from_date
        if self.to_date:
            filters['created__lte'] = self.to_date
        self.filters = filters

    def count(self):
        data = []
        userlist = self._get_userlist()
        filters = self.filters
        for user in userlist:
            filters['owner'] = user
            data.append(
                {
                'user_id': user.id,
                'username': user.username,
                'services': Service.objects.filter(**self.filters).count(),
                'variants': Variant.objects.filter(**self.filters).count(),
                'products': Product.objects.filter(**self.filters).count(),
            }
            )
        return data

    def _get_userlist(self):
        '''
        Returns users to display statistics for.
        For superuser: all users.
        For user: superuser stats + own stats.
        For anonymous user: superuser stats only
        '''
        if self.user.is_superuser:
            return User.objects.all()

        filters = {'is_superuser' : True}
        if self.user.is_authenticated:
            filters['id'] = self.user.id
        return User.objects.filter((Q(**filters, _connector=Q.OR)))



