from rest_framework import serializers

class PerformCreateMixin:
    '''
    Mixin class which prevents from duplicate codes being saved in a new object instance.
    '''
    def perform_create(self, serializer):
        code = serializer.validated_data['code']
        if self.model.objects.filter(code=code.upper()).exists():
            raise serializers.ValidationError(f"{self.model.__name__} code '{code.upper()}' already exists. Please try another one.")
        serializer.save(owner=self.request.user)