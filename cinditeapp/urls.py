from django.urls import path, include

from . import views

urlpatterns = [
    path('service/', include([
        path('', views.ServiceListView.as_view(), name='service'),
        path('create/', views.ServiceCreateView.as_view(), name="service-create"),
        path('update/<int:pk>', views.ServiceUpdateView.as_view(), name="service-update"),
        path('delete/<int:pk>', views.ServiceDeleteView.as_view(), name='service-delete'),
        path('<int:service_id>', views.ServiceProductListView.as_view(), name="service-product"),
        path('<int:pk>/add', views.ServiceProductAddDetailView.as_view(), name="service-product-add"),
        path('<int:service_id>/submit', views.submit, name="service-product-submit"),
        path('<int:service_id>/update/<int:pk>', views.ServiceProductUpdateView.as_view(), name="service-product-update"),
        path('<int:service_id>/delete/<int:pk>', views.ServiceProductDeleteView.as_view(), name="service-product-delete"),
    ])),
    path('variant/', include([
        path('', views.VariantListView.as_view(), name='variant'),
        path('create/', views.VariantCreateView.as_view(), name="variant-create"),
        path('update/<int:pk>', views.VariantUpdateView.as_view(), name="variant-update"),
        path('delete/<int:pk>', views.VariantDeleteView.as_view(), name='variant-delete')
    ])),
    path('index/', views.IndexListView.as_view(), name='index'),
    path('about/', views.About.as_view(), name='about'),
    path('api/', views.API.as_view(), name='api'),
    path('', views.LandingPageView.as_view(), name='landing')
]