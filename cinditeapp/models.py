from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import AbstractUser
from django.template.defaulttags import register

from cindite.settings import settings
from cinditeapp.querysets import CustomQuerySet, QSearch, QSearchIndex

# Create your models here.
class Service(models.Model):
    code = models.CharField(max_length=16)
    name = models.CharField(max_length=256)
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    objects = CustomQuerySet.as_manager()
    q_search = QSearch()

    class Meta:
        ordering = ['code']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        ''' Codes must be uppercase '''
        value = getattr(self, 'code', False)
        if value:
            setattr(self, 'code', value.upper())
        super().save(*args, **kwargs)

    @property
    def related_products(self):
        return self.product_set.all()

    @property
    def existing_variant_ids(self):
        return [product.variant_id for product in self.related_products]

    @register.filter
    def get_model_name(self):
        return self.model.__name__


class Variant(models.Model):
    code = models.CharField(max_length=8)
    name = models.CharField(max_length=256)
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    objects = CustomQuerySet.as_manager()
    q_search = QSearch()

    class Meta:
        ordering = ['code']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        ''' Codes must be uppercase '''
        value = getattr(self, 'code', False)
        if value:
            setattr(self, 'code', value.upper())
        super().save(*args, **kwargs)

    @register.filter
    def get_model_name(self):
        return self.model.__name__


class Index(models.Model):

    @classmethod
    def create(cls):
        '''
        Creates a new index entry and returns the instance.
        '''
        Index().save()
        return Index.objects.last()


class Product(models.Model):
    code = models.CharField(max_length=16)
    name = models.CharField(max_length=256)
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    variant = models.ForeignKey(Variant, on_delete=models.CASCADE)
    index = models.ForeignKey(Index, on_delete=models.CASCADE)

    objects = CustomQuerySet.as_manager()
    q_search = QSearchIndex()

    class Meta:
        ordering = ['code']

    def __str__(self):
        return self.name

    @register.filter
    def get_model_name(self):
        return self.model.__name__

    @classmethod
    def create_code(cls, service, variant):
        code = service.code + variant.code
        if Product.objects.filter(code=code).exists():
            raise ValidationError(f"Product {service.name} {variant.name} already exists. Please try another combination.")
        return service.code + variant.code

    @classmethod
    def create_many(cls, request, service):
        
        # create product variants specified in form
        variant_ids = request.POST.getlist('submit')
        variants = Variant.objects.filter(pk__in=variant_ids)
        
        index = Index.create()        
        product_list = [Product(
            code = Product.create_code(service, variant), 
            name = service.name + ' ' + variant.name,
            owner = request.user,
            service_id = service.pk,
            variant_id = variant.pk,
            index_id = index.pk
            ) for variant in variants]

        Product.objects.bulk_create(product_list)

    @classmethod
    def create_one(cls, request, service, variant):
        try: 
            code = Product.create_code(service, variant)
        except ValidationError as e: raise e
        name = service.name + ' ' + variant.name
        index = Index.create()
        product = Product.objects.create(
            code=code, 
            name=name,
            owner=request.user, 
            service=service, 
            variant=variant, 
            index=index,
            )
        return product


class User(AbstractUser):
    pass
