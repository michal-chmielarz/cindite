from django.apps import AppConfig


class CinditeappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cinditeapp'
