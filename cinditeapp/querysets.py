from django.db import models
from django.db.models import Q


class CustomQuerySet(models.QuerySet):
    '''
    Serves as database manager in models.
    '''
    def restrict(self, view):
        '''
        Returns user restricted content. 
        For superuser: all content.
        For user: superuser content + own content.
        For anonymous user: superuser content only
        '''
        if view.request.user.is_superuser:
            return self

        filters = {'owner__is_superuser' : True}
        if view.request.user.is_authenticated:
            filters['owner'] = view.request.user
        return self.filter(Q(**filters, _connector=Q.OR))


    def get_parent(self, view):
        '''
        Returns content related to parent service.
        '''
        service_id = view.kwargs.get('service_id')
        if service_id:
            return self.filter(service_id=service_id)
        return self


    def search(self, view):
        '''
        Returns searched content.
        '''
        query = view.request.GET.get('search')
        if query:
            return self.filter(view.model.q_search.get(query))
        return self


    def sort(self, view):
        '''
        Returns sorted content.
        '''
        sort = view.request.GET.get('sort')
        if sort:
            try:
                return self.order_by(sort)
            except Exception:
                return self
        return self


class QSearch:
    
    def get(self, query):
        '''
        Returns a Q object query defined in model.
        '''
        return Q(name__icontains=query, code__icontains=query, owner__username__icontains=query, _connector=Q.OR)

    @property
    def placeholder(self):
        """Placeholder for search input"""
        return 'Enter Code, Name or Owner...'


class QSearchIndex(QSearch):

    def get(self, query):
        '''
        Returns a Q object query defined in model.
        '''
        try:
            int(query)
            return Q(name__icontains=query, code__icontains=query, owner__username__icontains=query, index=query, _connector=Q.OR)
        except:
            return super().get(query)

    @property
    def placeholder(self):
        """Placeholder for search input"""
        return 'Enter Code, Name, Owner or Index no...'
