from django.forms import ModelForm, TextInput, PasswordInput, CharField
from django.contrib.auth.forms import AuthenticationForm, UsernameField, UserCreationForm

from cinditeapp.models import Service, Variant, Product, User


class CustomCreateForm(ModelForm):
    class Meta:
        model = None
        fields = ['code', 'name']
        widgets = {
            'code': TextInput(attrs={
                'id': 'code',
                'class': 'form-control',
                'placeholder': 'Enter Code...',
            }),
            'name': TextInput(attrs={
                'id': 'name',
                'class': 'form-control',
                'placeholder': 'Enter Name...',
            })
        }


class CustomUpdateForm(ModelForm):
    class Meta:
        model = None
        fields = ['code', 'name']
        widgets = {
            'code': TextInput(attrs={
                'id': 'code',
                'class': 'form-control',
                'placeholder': 'Enter Code...',
                'readonly': True,
            }),
            'name': TextInput(attrs={
                'id': 'name',
                'class': 'form-control',
                'placeholder': 'Enter Name...',
            })
        }


class ServiceCreateForm(CustomCreateForm):
    class Meta(CustomCreateForm.Meta):
        model = Service


class ServiceUpdateForm(CustomUpdateForm):
    class Meta(CustomUpdateForm.Meta):
        model = Service


class VariantCreateForm(CustomCreateForm):
    class Meta(CustomCreateForm.Meta):
        model = Variant


class VariantUpdateForm(CustomUpdateForm):
    class Meta(CustomUpdateForm.Meta):
        model = Variant


class ProductUpdateForm(CustomUpdateForm):
    class Meta(CustomUpdateForm.Meta):
        model = Product


class UserLoginForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    username = UsernameField(widget=TextInput(
        attrs={
            'id': 'username',
            'class': 'form-control mb-2',
            'placeholder': 'Username',
        }
    ))
    password = CharField(widget=PasswordInput(
        attrs={
            'id': 'password',
            'class': 'form-control mb-2',
            'placeholder': 'Password',
        }
    ))
    

class UserSignupForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields

    username = UsernameField(widget=TextInput(
        attrs={
            'id': 'username',
            'class': 'form-control',
            'placeholder': 'Username',
        }
    ), help_text='Letters, digits and @/./+/-/_ only.'
    )
    password1 = CharField(widget=PasswordInput(
        attrs={
            'id': 'password1',
            'class': 'form-control',
            'placeholder': 'Password',
        }
    ), help_text="At least 8 characters, can't be entirely numeric or too common."
    )
    password2 = CharField(widget=PasswordInput(
        attrs={
            'id': 'password2',
            'class': 'form-control',
            'placeholder': 'Confirm Password',
        }
    ), help_text='Enter the same password as before, for verification.'
    )