from django.http import HttpResponseRedirect
from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.shortcuts import render


class QuerySetMixin:
    '''
    Mixin class which narrows down queryset lookup in Views.
    Calls methods from CustomQuerySet instance.
    '''
    def get_queryset(self):
        
        # 'self' is a View instance
        view = self
        # 'objects' is a CustomQuerySet database manager
        return self.model.objects.restrict(view).get_parent(view).search(view).sort(view)


class FormAuthenticationTestMixin:
    '''
    Authentication test at form level. 
    Redirects to login page if user is not authenticated.
    '''
    def form_valid(self, form):
        if self.request.user.is_authenticated:
            form.instance.owner = self.request.user
            return super().form_valid(form)
        return HttpResponseRedirect(settings.LOGIN_URL + '?next=' + self.request.path)


def func_authentication_test(view):
    '''
    Authentication test as a method wrapper.
    Redirects to login page if user is not authenticated.
    '''
    def wrapper(*args, **kwargs):

        request, *_ = args
        if request.user.is_authenticated:
            return view(*args, **kwargs)
        return HttpResponseRedirect(settings.LOGIN_URL + '?next=' + request.path)

    return wrapper


class PermissionTestMixin:
    '''
    Permission test at dispatch level.
    Raises 'Permission Denied' if user is neither an owner nor a superuser.
    '''
    def dispatch(self, request, *args, **kwargs):
        user = self.request.user
        owner = self.get_object().owner

        if user == owner or user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied


class PreventDuplicateCodesMixin:
    '''
    Prevents duplicate codes from being created.
    '''
    def form_valid(self, form):
        model_name = str(self.model.get_model_name(self))

        if self.model.objects.filter(code=form.instance.code.upper()).exists():
            return render(self.request, 'cinditeapp/code_exists.html', 
                {'code': form.instance.code, 'model_name': model_name, 'redirect': model_name.lower()+'-create'}
                )
        return super().form_valid(form)