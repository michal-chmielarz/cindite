from django.template.defaulttags import register

@register.simple_tag(takes_context=True)
def update_url(context, **kwargs):
    '''
    Updates/adds url GET parameters and does not forget pre-existing.
    Reference: https://gist.github.com/benbacardi/d6cd0fb8c85e1547c3c60f95f5b2d5e1

    E.g: given the querystring ?foo=1&bar=2
    {% update_url_get bar=3 %} outputs ?foo=1&bar=3
    {% update_url_get foo='baz' %} outputs ?foo=baz&bar=2
    {% update_url_get foo='one' bar='two' baz=99 %} outputs ?foo=one&bar=two&baz=99
    '''

    query_dict = context['request'].GET.copy()
    for k, v in kwargs.items():
        query_dict[k] = v

    return query_dict.urlencode()