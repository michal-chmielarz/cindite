from django.shortcuts import render, get_object_or_404

from django.http import HttpResponseRedirect, Http404
from django.urls import reverse, reverse_lazy

from django.views import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.contrib.auth.views import LoginView
from django.contrib.auth import authenticate, login
from django.contrib.auth import get_user_model

from django.conf import settings

from cinditeapp.forms import ServiceCreateForm, ServiceUpdateForm, VariantCreateForm, VariantUpdateForm, ProductUpdateForm, UserLoginForm, UserSignupForm
from cinditeapp.models import Service, Product, Variant, Index
from cinditeapp.mixins import QuerySetMixin, FormAuthenticationTestMixin, func_authentication_test, PreventDuplicateCodesMixin, PermissionTestMixin


# get user model
User = get_user_model()


# Create your views here.
class ServiceListView(QuerySetMixin, ListView):
    model = Service
    template_name = 'cinditeapp/service.html'
    context_object_name = 'services'
    paginate_by = 10


class ServiceCreateView(FormAuthenticationTestMixin, PreventDuplicateCodesMixin, CreateView):
    model = Service
    form_class = ServiceCreateForm
    template_name = 'cinditeapp/service-create.html'
    success_url = reverse_lazy('service')


class ServiceUpdateView(PermissionTestMixin, UpdateView):
    model = Service
    form_class = ServiceUpdateForm
    template_name = 'cinditeapp/service-update.html'
    success_url = reverse_lazy('service')


class ServiceDeleteView(PermissionTestMixin, DeleteView):
    model = Service
    success_url = reverse_lazy('service')


class ServiceProductListView(QuerySetMixin, ListView):
    model = Product
    template_name = 'cinditeapp/service-product.html'
    context_object_name = 'products'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        '''
        Provides parent Service for product list.
        Service has to be visible even if there is no products on the list.
        '''
        context = super().get_context_data(**kwargs)
        try:
            context['service'] = Service.objects.restrict(self).get(pk=self.kwargs['service_id'])
        except Service.DoesNotExist: raise Http404
        return context


class ServiceProductAddDetailView(QuerySetMixin, DetailView):
    model = Service
    template_name = 'cinditeapp/service-product-add.html'
    context_object_name = 'service'

    def get_context_data(self, **kwargs):
        '''
        Prevents duplicate products from being created.
        Existing service variants are not presented to user as available options.
        '''
        context = super().get_context_data(**kwargs)
        context['variants'] = Variant.objects.restrict(self).exclude(pk__in=self.object.existing_variant_ids).order_by('name')
        return context


@func_authentication_test
def submit(request, service_id):

    service = get_object_or_404(Service, pk=service_id)
    Product.create_many(request, service)

    return HttpResponseRedirect(reverse('service-product', args=(service.id,)))


class ServiceProductUpdateView(PermissionTestMixin, UpdateView):
    model = Product
    form_class = ProductUpdateForm
    template_name = 'cinditeapp/service-product-update.html'

    def get_success_url(self, **kwargs):
        service_id = self.object.service_id
        return reverse_lazy('service-product', kwargs = {'service_id': service_id})


class ServiceProductDeleteView(PermissionTestMixin, DeleteView):
    model = Product

    def get_success_url(self, **kwargs):
        service_id = self.object.service_id
        return reverse_lazy('service-product', kwargs = {'service_id': service_id})


class VariantListView(QuerySetMixin, ListView):
    model = Variant
    template_name = 'cinditeapp/variant.html'
    context_object_name = 'variants'
    paginate_by = 10


class VariantCreateView(FormAuthenticationTestMixin, PreventDuplicateCodesMixin, CreateView):
    model = Variant
    form_class = VariantCreateForm
    template_name = 'cinditeapp/variant-create.html'
    success_url = reverse_lazy('variant')


class VariantUpdateView(PermissionTestMixin, UpdateView):
    model = Variant
    form_class = VariantUpdateForm
    template_name = 'cinditeapp/variant-update.html'
    success_url = reverse_lazy('variant')


class VariantDeleteView(PermissionTestMixin, DeleteView):
    model = Variant
    success_url = reverse_lazy('variant')


class IndexListView(QuerySetMixin, ListView):
    model = Product
    template_name = 'cinditeapp/index.html'
    context_object_name = 'products'
    paginate_by = 15


class LandingPageView(View):

    def get(self, request):
        return render(request, 'cinditeapp/landing.html', {})


class About(View):

    def get(self, request):
        return render(request, 'cinditeapp/about.html', {})


class API(View):

    def get(self, request):
        return render(request, 'cinditeapp/api.html', {})


class SignUpView(View):

    def get(self, request):
        form = UserSignupForm()
        context = {
            'form': form,
            'next': request.GET.get('next'),
        }
        return render(request, 'registration/signup.html', context)

    def post(self, request):
        form = UserSignupForm(request.POST)
        if form.is_valid():
            form.save() # we create user here
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return HttpResponseRedirect(request.POST.get('next', settings.LOGIN_REDIRECT_URL))
        return render(request, 'registration/signup.html', {'form': form})


class CustomLoginView(LoginView):
    authentication_form = UserLoginForm



