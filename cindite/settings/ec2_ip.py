'''
Connects to AWS EC2 instance metadata API and pulls current IP address for health checks.
Based on https://stackoverflow.com/questions/63011195/how-to-resolve-aws-elastic-beanstalk-django-health-check-problems
'''
import requests


def get_token():
    """Set the autorization token to live for 6 hours (maximum)"""
    headers = {
        'X-aws-ec2-metadata-token-ttl-seconds': '21600',
    }
    response = requests.put('http://169.254.169.254/latest/api/token', headers=headers)
    return response.text


def get_ec2_private_ip():
    """See https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-data-retrieval.html"""

    try:
        token = get_token()
        headers = {
            'X-aws-ec2-metadata-token': f"{token}",
        }
        response = requests.get('http://169.254.169.254/latest/meta-data/local-ipv4', headers=headers)
        return response.text
    except Exception:
        return None